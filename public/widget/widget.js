function loadScript(url) {
    var scriptTag = document.createElement('script');
    scriptTag.setAttribute("type", "text/javascript");
    scriptTag.setAttribute("src", url);
    var head = document.getElementsByTagName('head')[0];
    head.appendChild(scriptTag);
}

function loadCss(url) {
    let linkTag = document.createElement('link');
    linkTag.setAttribute('rel', 'stylesheet');
    linkTag.setAttribute('href', url);
    var head = document.getElementsByTagName('head')[0];
    head.appendChild(linkTag);
}

let SERVER = 'http://localhost:3000/';

loadScript("https://use.fontawesome.com/721f564020.js");
loadScript("https://cdn.jsdelivr.net/npm/opus-media-recorder@latest/OpusMediaRecorder.umd.js")
loadScript("https://cdn.jsdelivr.net/npm/opus-media-recorder@latest/encoderWorker.umd.js");
loadCss("https://cdnjs.cloudflare.com/ajax/libs/three-dots/0.2.3/three-dots.min.css");
loadCss("https://fonts.cdnfonts.com/css/pt-sans-2");
loadCss(SERVER + "widget/widget.css");

fetch(SERVER + 'widget/widget.html', {
        method: 'get'
    }).then(resp => resp.text()).then((html) => {
        let obj = document.createElement('div');
        obj.innerHTML = html;
        document.body.appendChild(obj);
    }).catch((err) => {
        console.error(err);
    });

let started = false;

function start() {
    if (started) {
        return;
    }
    started = true;

    console.log('started');

    if (!(window.MediaRecorder && window.MediaRecorder.isTypeSupported('audio/ogg;codecs=opus'))) {
        console.log('media', window.MediaRecorder && window.MediaRecorder.isTypeSupported('audio/ogg;codecs=opus'));
        window.MediaRecorder = OpusMediaRecorder;
    }
    
    let kzOpened = false;

    const kzBtn = document.getElementById('kz-btn');
    const kzDialogue = document.getElementById('kz-dialogue');
    const kzDialogueText = document.getElementById('kz-dialogue-text');
    const microEl = kzBtn.getElementsByClassName('kz-icon-micro')[0];
    const dotsEl = kzBtn.getElementsByClassName('kz-icon-dots')[0];

    const workerOptions = {
        OggOpusEncoderWasmPath: 'https://cdn.jsdelivr.net/npm/opus-media-recorder@latest/OggOpusEncoder.wasm'
    };

    let recorder;
    let audioChunks = [];
    let isCanSending = true;

    const createStatus = status => `Status <span style="color: red;">${status}</span>`;

    const handleAudioStream = (stream) => {
        recorder = new MediaRecorder(stream, {mimeType: 'audio/ogg;codecs=opus'}, workerOptions);
        recorder.addEventListener('dataavailable', e => {
            const file = new File([e.data], "speech.ogg", { type: 'audio/ogg;codecs=opus' });
            audioChunks = [];
            const formData = new FormData();
            formData.append('speech', file, 'speech.ogg');

            audioChunks.push(formData);

            if(isCanSending && recorder.state === 'recording') {
                isCanSending = false;
                fetch('http://localhost:3000/api/voice', {
                    method: 'post',
                    body: audioChunks.shift(),
                }).then(resp => resp.json()).then(({isFinal, result, url}) => {
                    if(isFinal && recorder.state === 'recording') {
                        recorder.stop();
                        kzOpened = false;
                        microEl.classList.remove('kz-hidden');
                        dotsEl.classList.add('kz-hidden');

                        console.log('Final result: ', result);
                        console.log('Final url: ', url);

                        if (url) {
                            window.location.href = url;
                        }
                    }
                    isCanSending = true;
                    kzDialogueText.innerHTML = result;
                }).catch((err) => {
                    console.error(err);
                });
            }
        });
    };

    kzBtn.addEventListener('click', (event) => {
        if (kzOpened || !recorder) {
            return;
        }

        kzOpened = true;
        kzDialogue.classList.remove('kz-hidden');
        microEl.classList.add('kz-hidden');
        dotsEl.classList.remove('kz-hidden');

        recorder.start(400);
    });

    navigator.mediaDevices.getUserMedia({audio: true}).then(handleAudioStream);
}

setTimeout(start, 1000);

window.addEventListener('load', function() {
    start();
});