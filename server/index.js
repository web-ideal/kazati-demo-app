const path = require('path');
const speechToText = require('./sst/main');
//const textToSpeech = require('./tts/main');
const getDnsUrl = require('./controllers/dns-controller');
const phaseController = require('./controllers/phase-controller');

const express = require('express');
const multer = require('multer');

var app = express();
var cors = require('cors');

app.use(
    cors({
        credentials: true,
        origin: true
    })
);
app.options('*', cors());

app.listen((process.env.PORT || 3000), () => {
    console.log("Server running on port ", (process.env.PORT || 3000));
})

app.use(express.static('public'));

// app.get('/', (req, res) => {
//     res.sendFile(path.join(__dirname, '../public/index.html'));
// })

// app.post("/api/voice", multer().single('speech'), async (req, res, next) => {
//     console.log(req.file);
//     var textResult = await speechToText(req.file.buffer);
//     console.log('Result: ', textResult);
//     textResult.url = getDnsUrl(textResult.result);
//     res.json(textResult);
// })

app.post("/api/v2/voice", multer().single('speech'), async (req, res, next) => {
    console.log(req.file);
    var textResult = await speechToText(req.file.buffer);

    const result = {
        result: phaseController(textResult.result),
        text: textResult.result,
        final: textResult.isFinal
    };

    console.log('Final: ', result.final);
    console.log('Text: ', result.text);
    console.log('Result: ', result.result);

    res.json(result);
})