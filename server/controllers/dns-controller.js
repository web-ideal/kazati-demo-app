const dnsHost = 'https://www.dns-shop.ru/';

const dnsUrls = {
    base: () => '',
    urls: {

        'noutbuki': {
            base: () => 'catalog/17a892f816404e77/noutbuki/?',
            urls: {

                'stock': {
                    base: (text) => {
                        let res = '&stock='
                        if (text.search('налич') != -1 || text.search('сейчас') != -1) {
                            res += 'now-';
                        }
                        if (text.search('сегодня') != -1) {
                            res += 'today-';
                        }
                        if (text.search('завтра') != -1) {
                            res += 'tomorrow-';
                        }
                        return res;
                    }
                },

                'price': {
                    base: (text) => {
                        let fromMatch = [ ...text.matchAll(/от ([\d]+) р/g) ].pop();
                        let toMatch = [ ...text.matchAll(/до ([\d]+) р/g) ].pop();
                        let from = fromMatch ? fromMatch.pop() : '0' ;
                        let to = toMatch ? toMatch.pop() : '999999' ;
                        return '&price=' + from + '-' + to;
                    }
                },

                'processor': {
                    base: (text) => {
                        let res = '&f[66f]=';
                        if (text.search('интел') != -1 || text.search('интэл') != -1) { res += '268s-'; }
                        if (text.search('амд') != -1) { res += '268t-'; }
                        if (text.search('апл') != -1 || text.search('эпл') != -1) { res += 'wxg11-'; }
                        return res;
                    }
                },

                'ram': {
                    base: (text) => {
                        let res = '&f[676]=';
                        let match = [ ...text.matchAll(/([\d]+) (?:гигабайт|гб)/g) ].pop();
                        let value = match ? match.pop() : undefined;

                        if (value == '2') { res += '26o4-' }
                        if (value == '4') { res += '26o6-' }
                        if (value == '8') { res += '26o8-' }
                        if (value == '12') { res += '26oa-' }
                        if (value == '16') { res += '26o9-' }
                        if (value == '32') { res += '26ob-' }
                        return res;
                    }
                },

                'sorting': {
                    base: (text) => {

                    }
                }

            },
            filter: function (text) {
                return [ this.urls.stock, this.urls.price, this.urls.processor, this.urls.ram ];
            }
        },

        'smartfony': {
            base: () => 'catalog/17a8a01d16404e77/smartfony/',
            urls: {

            },
            filter: function (text) {
                return [];
            }
        }

    },
    filter: function (text) {
        if (text.search('ноут') != -1) {
            console.log(this);
            return [ this.urls.noutbuki ];
        } else if (text.search('смартфон') != -1 || text.search('телефон') != -1) {
            return [ this.urls.smartfony ];
        }
        return [];
    }
};

function filter(text, state) {
    let url = state.base(text);
    if (!state.filter) {
        return url;
    }

    let childs = state.filter(text);

    for (let i = 0; i < childs.length; ++i) {
        let child = childs[i];
        url += filter(text, child);
    }

    return url;
}

// let text = "Хочу купить ноутбук в наличии от 50000 р до 100000 р от 70000 р до 120000 р c оперативкой от 8 гигабайт";
// let url = filter(text.toLowerCase(), dnsUrls);
// console.log('DNS URL: ', url);

module.exports = function (text) {
    if (!text) {
        text = '';
    }

    try {
        let url = filter(text.toLowerCase(), dnsUrls);
        if (url) {
            url = dnsHost + url;
        }
        console.log('DNS URL: ', url);
        return url;
    } catch(err) {
        console.error(err);
    }
};