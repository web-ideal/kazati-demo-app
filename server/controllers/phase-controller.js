
const findLastMatchFromTo = (matches, min, max) => {
    return matches.reduce((res, m) => {
        return m.every((x, i) => i === 0 || (!min || min <= parseInt(x)) && (!max || parseInt(x) <= max))
            ? m
            : res;
    }, undefined);
}

const findValueFromTo = (match, fromToMatch, fromMatch, toMatch, min, max) => {
    const lastPos = match ? match.index + match[0].length : -1;

    if (fromToMatch && match) {
        const fromToLastPos = fromToMatch.index + fromToMatch[0].length;

        if (fromToLastPos === lastPos) {
            let from = parseInt(fromToMatch[1]);
            let to = parseInt(fromToMatch[2]);
            return (!min || min <= Math.max(from, to)) && (!max || Math.min(from, to) <= max)
                ? { from: Math.min(from, to), to: Math.max(from, to) }
                : {};
        }
    }

    if (fromMatch && toMatch && match) {
        const fromLastPos = fromMatch.index + fromMatch[0].length;
        const toLastPos = toMatch.index + toMatch[0].length;

        if (fromLastPos === lastPos || toLastPos === lastPos) {
            let from = parseInt(fromMatch[1]);
            let to = parseInt(toMatch[1]);
            return (!min || min <= Math.max(from, to)) && (!max || Math.min(from, to) <= max)
                ? { from: Math.min(from, to), to: Math.max(from, to) }
                : {};
        }
    }

    if (fromMatch && match) {
        const fromLastPos = fromMatch.index + fromMatch[0].length;

        if (fromLastPos === lastPos) {
            let from = parseInt(fromMatch[1]);
            return (!min || min <= from) && (!max || from <= max)
                ? { from } 
                : {};
        }
    }

    if (toMatch && match) {
        const toLastPos = toMatch.index + toMatch[0].length;

        if (toLastPos === lastPos) {
            let to = parseInt(toMatch[1]);
            return (!min || min <= to) && (!max || to <= max) 
                ? { to } 
                : {};
        }
    }

    if (match) {
        let value = parseInt(match[1]);
        return (!min || min <= value) && (!max || value <= max) 
            ? { from: value, to: value + 0.9 } 
            : {};
    }
}


const stockController = {
    
    run: function(text) {
        const result = [];

        if (text.search('налич') != -1 || text.search('сейчас') != -1) {
            result.push('now');
        }
        if (text.search('сегодня') != -1) {
            result.push('today');
        }
        if (text.search('завтра') != -1) {
            result.push('tomorrow');
        }
        console.log('Stock', result);
        return result.length ? result : undefined;
    }
}

const brandController = {

    run: function(text) {
        const appleMatch = [...text.matchAll(/(?:эппл|аппл|эпл|апл|апле|аппле)/g)].pop();
        const asusMatch = [...text.matchAll(/(?:асус|эсус)/g)].pop();
        const hpMatch = [...text.matchAll(/(?:эйч пи|эйчпи| хп |$хп | хп$|^хп$)/g)].pop();
        const lenovoMatch = [...text.matchAll(/(?:леново|линово)/g)].pop();
        const irbisMatch = [...text.matchAll(/(?:ирбис)/g)].pop();

        const result = [];
        if (appleMatch) { result.push('apple'); }
        if (asusMatch) { result.push('asus'); }
        if (hpMatch) { result.push('hp'); }
        if (lenovoMatch) { result.push('lenovo'); }
        if (irbisMatch) { result.push('irbis'); }
        
        return result;
    }
}

const priceController = {

    run: function(text) {
        const fromMatch = [ ...text.matchAll(/от ([\d]+) р/g) ].pop();
        const toMatch = [ ...text.matchAll(/до ([\d]+) р/g) ].pop();

        const from = fromMatch ? fromMatch.pop() : '' ;
        const to = toMatch ? toMatch.pop() : '' ;

        return { from, to };
    }
}

const classificationController = {

    run: function(text) {

        const osController = {
            
            run: function (text) {
                const macosMatch = [...text.matchAll(/(?: мак |^мак | мак$|^мак$|на маке|макос|мак ос|макбук)/g)].pop();
                const windowsMatch = [...text.matchAll(/(?:винда|винде|винду|виндовс|финдоус|виндс)/g)].pop();
                const result = [];

                if (macosMatch) {
                    result.push('macos');
                }
                if (windowsMatch) {
                    result.push('windows');
                }

                return result;
            }
        }
        
        const isGamingController = {
            
            run: function (text) {
                const notGamingMatch = [...text.matchAll(/(?:не игровой|неигровой|не для игр|не чтобы играть|работ|учеб)/g)].pop();
                const gamingMatch = [ ...text.matchAll(/(?:игровой|для игр|чтобы играть)/g) ].pop();

                if (gamingMatch && notGamingMatch) {
                    const lastGamingPos = gamingMatch.index + gamingMatch[0].length;
                    const lastNotGamingPos = notGamingMatch.index + notGamingMatch[0].length;
                    return [ lastGamingPos > lastNotGamingPos ];
                } else if (gamingMatch && !notGamingMatch) {
                    return [ true ];
                } else if (!gamingMatch && notGamingMatch) {
                    return [ false ];
                }

                return undefined;
            }
        }

        return {
            os: osController.run(text),
            isGaming: isGamingController.run(text)
        }
    }
}

const screenController = {

    run: function(text) {

        const typeController = {
            
            run: function (text) {
                const ipsMatch = [...text.matchAll(/(?:ай пи эс|ай пи си|айпс|айпээс|айпэс|айпиэс|айписи|ай пэ эс|ай пэс|эй пи эс|эй пи си|эйпс|эйпээс|эйпэс|эйпиэс|эйписи|эй пэ эс|эй пэс|ипээс|эпиэс)/g)].pop();
                const oledMatch = [...text.matchAll(/(?:олет|олэд|олед)/g)].pop();
                const tnfilmMatch = [...text.matchAll(/(?:ти эн фильм|ти эн|тиэн фильм|тиэн фильм|тэн фильм)/g)].pop();
                
                const result = [];
                if (ipsMatch) { result.push('ips') }
                if (oledMatch) { result.push('oled') }
                if (tnfilmMatch) { result.push('tn+film') }
                
                return result;
            }
        }
        
        const diagonalController = {
            
            run: function (text) {

                const fromToDiagonalMatches = [ ...text.matchAll(/от ([\d]+) до ([\d]+) дюймов/g) ];
                const fromDiagonalMatches = [ ...text.matchAll(/от ([\d]+) дюймов/g) ];
                const toDiagonalMatches = [ ...text.matchAll(/до ([\d]+) дюймов/g) ];
                const diagonalMatches = [ ...text.matchAll(/([\d]+) дюймов/g) ];
                
                const fromToDiagonalMatch = findLastMatchFromTo(fromToDiagonalMatches);
                const fromDiagonalMatch = findLastMatchFromTo(fromDiagonalMatches);
                const toDiagonalMatch = findLastMatchFromTo(toDiagonalMatches);
                const diagonalMatch = findLastMatchFromTo(diagonalMatches);

                return findValueFromTo(diagonalMatch, fromToDiagonalMatch, fromDiagonalMatch, toDiagonalMatch);
            }
        }

        const resolutionController = {
            
            run: function (text) {

            }
        }

        return {
            type: typeController.run(text),
            diagonal: diagonalController.run(text),
            resolution: resolutionController.run(text),
        }
    }
}

const processorController = {

    run: function(text) {

        const brandController = {
            
            run: function (text) {
                const appleMatch = [...text.matchAll(/(?:эппл|аппл|эпл|апл|апле|аппле)/g)].pop();
                const intelMatch = [...text.matchAll(/(?:интел|интэл)/g)].pop();
                const amdMatch = [...text.matchAll(/(?:амд|эй эм дэ|эмд|эй мд)/g)].pop();

                let result = [];
                if (appleMatch) {
                    result.push('apple');
                }
                if (intelMatch) {
                    result.push('intel');
                }
                if (amdMatch) { 
                    result.push('amd');
                }

                return result;
            }
        }
        
        const coreController = {
            
            run: function (text) {
                
                const fromToCoreMatches = [ ...text.matchAll(/от ([\d]+) до ([\d]+) (?:ядра|ядер)/g) ];
                const fromCoreMatches = [ ...text.matchAll(/от ([\d]+) (?:ядра|ядер)/g) ];
                const toCoreMatches = [ ...text.matchAll(/до ([\d]+) (?:ядра|ядер)/g) ];
                const coreMatches = [ ...text.matchAll(/([\d]+) (?:ядра|ядер)/g) ];
                
                const fromToCoreMatch = findLastMatchFromTo(fromToCoreMatches);
                const fromCoreMatch = findLastMatchFromTo(fromCoreMatches);
                const toCoreMatch = findLastMatchFromTo(toCoreMatches);
                const coreMatch = findLastMatchFromTo(coreMatches);

                return findValueFromTo(coreMatch, fromToCoreMatch, fromCoreMatch, toCoreMatch);
            }
        }

        return {
            brand: brandController.run(text),
            core: coreController.run(text),
        }
    }
}

const ramController = {

    run: function(text) {

        const memoryController = {
            
            run: function (text) {

                const fromToMemoryMatches = [ ...text.matchAll(/от ([\d]+) до ([\d]+) (?:гигабайт|гига|гигов|гб)/g) ];
                const fromMemoryMatches = [ ...text.matchAll(/от ([\d]+) (?:гигабайт|гига|гигов|гб)/g) ];
                const toMemoryMatches = [ ...text.matchAll(/до ([\d]+) (?:гигабайт|гига|гигов|гб)/g) ];
                const memoryMatches = [ ...text.matchAll(/([\d]+) (?:гигабайт|гига|гигов|гб)/g) ];

                const fromToMemoryMatch = findLastMatchFromTo(fromToMemoryMatches, 0, 32);
                const fromMemoryMatch = findLastMatchFromTo(fromMemoryMatches, 0, 32);
                const toMemoryMatch = findLastMatchFromTo(toMemoryMatches, 0, 32);
                const memoryMatch = findLastMatchFromTo(memoryMatches, 0, 32);

                return findValueFromTo(memoryMatch, fromToMemoryMatch, fromMemoryMatch, toMemoryMatch, 0, 32);
            }
        }

        return {
            memory: memoryController.run(text),
        }
    }
}

const storageController = {

    run: function(text) {

        const memoryController = {
            
            run: function (text) {
                const fromToMemoryMatches = [ ...text.matchAll(/от ([\d]+) до ([\d]+) (?:гигабайт|гига|гигов|гб)/g) ];
                const fromMemoryMatches = [ ...text.matchAll(/от ([\d]+) (?:гигабайт|гига|гигов|гб)/g) ];
                const toMemoryMatches = [ ...text.matchAll(/до ([\d]+) (?:гигабайт|гига|гигов|гб)/g) ];
                const memoryMatches = [ ...text.matchAll(/([\d]+) (?:гигабайт|гига|гигов|гб)/g) ];

                const fromToMemoryMatch = findLastMatchFromTo(fromToMemoryMatches, 64);
                const fromMemoryMatch = findLastMatchFromTo(fromMemoryMatches, 64);
                const toMemoryMatch = findLastMatchFromTo(toMemoryMatches, 64);
                const memoryMatch = findLastMatchFromTo(memoryMatches, 64);

                return findValueFromTo(memoryMatch, fromToMemoryMatch, fromMemoryMatch, toMemoryMatch, 64);
            }
        }
        
        const typeController = {
            
            run: function (text) {
                
            }
        }

        return {
            memory: memoryController.run(text),
            type: typeController.run(text),
        }
    }
}

const batteryController = {

    run: function(text) {

        const hoursController = {
            
            run: function (text) {

            }
        }

        return {
            hours: hoursController.run(text),
        }
    }
}

const phaseController = {

    run: function(text) {
        const result = {
            stock: stockController.run(text),
            brand: brandController.run(text),
            price: priceController.run(text),
            classification: classificationController.run(text),
            screen: screenController.run(text),
            processor: processorController.run(text),
            ram: ramController.run(text),
            storage: storageController.run(text),
            battery: batteryController.run(text)
        }
        return result;
    }
}

module.exports = function(text) {
    return phaseController.run(text.toLowerCase());
}